#include <iostream>
#include <conio.h>
#include <string>

enum Suit {
	HEARTS,
	DIAMONDS,
	SPADES,
	CLUBS
};

enum Rank {
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King, 
	Ace
};

struct Card {
	Rank rank;
	Suit suit;
	int value;
};

int main() {

	_getch();
	return 0;
};